json.extract! record, :id, :hours, :description, :project_id, :created_at, :updated_at
json.url record_url(record, format: :json)
