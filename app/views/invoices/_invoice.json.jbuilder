json.extract! invoice, :id, :tot_hours, :cost_per_hour, :description, :customer_id, :created_at, :updated_at
json.url invoice_url(invoice, format: :json)
