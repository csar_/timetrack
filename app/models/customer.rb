class Customer < ApplicationRecord
has_many :projects
has_many :invoices
belongs_to :user

validates :name, presence:true
validates :user_id, presence:true, numericality: {only_integer: true}

end

