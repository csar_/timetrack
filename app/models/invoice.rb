class Invoice < ApplicationRecord
belongs_to :customer
validates :tot_hours, presence:true, numericality: { only_integer: true}
validates :cost_per_hour, presence:true, numericality: true
validates :description, presence:true
validates :customer_id, presence:true ,numericality: { only_integer: true}
end
