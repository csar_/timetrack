class Record < ApplicationRecord
   belongs_to :project

   validates :project_id, presence:true
   validates :hours, presence:true, numericality: true
end
