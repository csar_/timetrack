class Project < ApplicationRecord
   has_many :records
   belongs_to :customer
   belongs_to :user

   validates :name, presence:true
   validates :description, presence:true
   validates :user_id, presence:true, numericality: { only_integer: true}
   validates :customer_id, presence:true, numericality: { only_integer: true}
end
