class SiteController < ApplicationController
  def index
    if user_signed_in?
      @projects = current_user.projects
      @customers = current_user.customers
    end
  end
end
