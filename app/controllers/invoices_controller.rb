class InvoicesController < ApplicationController
  before_action :set_invoice, only: [:show, :edit, :update, :destroy]
  before_action :authenticate_user!

  def index
    @customer = Customer.find(params[:customer_id])
    @invoices = @customer.invoices    
  end

  def show
  end

  def new
    if params[:project_id].nil?
     @tot_hours=0
     @customer = Customer.find(params[:customer_id])
     @customer.projects.each do |p|
       logger.debug "Record: #{p.records.sum(:hours).to_i}"
       @tot_hours+=p.records.sum(:hours).to_i
     end

    else
     @project = Project.find(params[:project_id])
     @customer = @project.customer
     @tot_hours = @project.records.sum(:hours)
    end

    @invoice = Invoice.new
    @invoice.tot_hours = @tot_hours
  end

  def edit
    @customer = Customer.find(params[:customer_id]) 
    @tot_hours = @invoice.tot_hours
  end

  def create
    @invoice = Invoice.new(invoice_params)

    respond_to do |format|
      if @invoice.save
        format.html { redirect_to root_path, notice: 'Invoice was successfully created.' }
        format.json { render :show, status: :created, location: @invoice }
      else
        format.html {
	        if invoice_params[:customer_id]	
	        @customer = Customer.find(invoice_params[:customer_id])
		end
                if invoice_params[:project_id]
		@project = Project.find(invoice_params[:project_id])
                end
		render :new
	}
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    respond_to do |format|
      if @invoice.update(invoice_params)
        format.html { redirect_to root_path, notice: 'Invoice was successfully updated.' }
        format.json { render :show, status: :ok, location: @invoice }
      else
        format.html { render :edit }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @invoice.destroy
    respond_to do |format|
      format.html { redirect_to root_path, notice: 'Invoice was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invoice
      @invoice = Invoice.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invoice_params
      params.require(:invoice).permit(:tot_hours, :cost_per_hour, :description, :customer_id, :project_id)
    end
end
