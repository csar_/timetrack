class CreateInvoices < ActiveRecord::Migration[5.2]
  def change
    create_table :invoices do |t|
      t.integer :tot_hours
      t.float :cost_per_hour
      t.string :description
      t.integer :customer_id

      t.timestamps
    end
  end
end
