class CreateRecords < ActiveRecord::Migration[5.2]
  def change
    create_table :records do |t|
      t.string :hours
      t.string :description
      t.integer :project_id

      t.timestamps
    end
  end
end
