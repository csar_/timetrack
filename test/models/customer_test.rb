require 'test_helper'

class CustomerTest < ActiveSupport::TestCase
   test "should not create it" do
     customer = Customer.new
     assert_not customer.save
   end

   test "should create it" do
     customer = Customer.new 
     customer.user = User.find(1)
     customer.name = 'Test Customer'
     assert customer.save
   end

end
