require 'test_helper'

class RecordTest < ActiveSupport::TestCase
   test "should not create it" do
     record = Record.new
     assert_not record.save
   end

   test "should create it" do
     record = Record.new
     record.project = Project.find(1)
     record.hours = 10
     record.description = 'Test Desc'
     assert record.save
   end

   test "wrong input on hours" do
     record = Record.new
     record.project = Project.find(1)
     record.hours = '#'
     record.description = 'Test Desc'
     assert_not record.save
   end
end
