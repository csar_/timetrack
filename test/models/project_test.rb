require 'test_helper'

class ProjectTest < ActiveSupport::TestCase
   test "should not create it" do
     project = Project.new
     assert_not project.save
   end

   test "should create it" do
     project = Project.new
     project.customer = Customer.find(1)
     project.user = User.find(1)
     project.name = 'Test Project'
     project.description = 'Test Desc'
     assert project.save
   end
end
