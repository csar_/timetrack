require 'test_helper'

class InvoiceTest < ActiveSupport::TestCase
   test "should not create it" do
     invoice = Invoice.new
     assert_not invoice.save
   end

   test "should create it" do
     invoice = Invoice.new
     invoice.customer = Customer.find(1)
     invoice.tot_hours = 10
     invoice.cost_per_hour = 10.0
     invoice.description = 'Test Desc'
     assert invoice.save
   end

   test "wrong input in tot_hours" do
     invoice = Invoice.new
     invoice.customer = Customer.find(1)
     invoice.tot_hours = 'ì'
     invoice.cost_per_hour = 10.0
     invoice.description = 'Test Desc'
     assert_not invoice.save
   end

   test "wrong input in cost_per_hour" do
     invoice = Invoice.new
     invoice.customer = Customer.find(1)
     invoice.tot_hours = 10
     invoice.cost_per_hour = 'ciao'
     invoice.description = 'Test Desc'
     assert_not invoice.save
   end

end
