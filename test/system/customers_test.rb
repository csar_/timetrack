require "application_system_test_case"

class CustomersTest < ApplicationSystemTestCase
include Devise::Test::IntegrationHelpers
  setup do
    @customer = customers(:one)
    @user = users(:one)
  end

  test "visiting the index without auth" do
    visit customers_url
    assert_no_selector "h1", text: "Customers"
  end

   test "visiting the index" do
    sign_in @user
    visit customers_url
    assert_selector "h1", text: "Customers"
  end

  test "creating a Customer" do
    sign_in @user
    visit new_customer_url

    fill_in "Name", with: @customer.name
    click_on "Create Customer"

    assert :success
  end

  test "updating a Customer" do
    sign_in @user
    visit customers_url
    click_on "Edit", match: :first

    fill_in "Name", with: @customer.name
    click_on "Update Customer"
    
    assert :success
  end

  test "destroying a Customer" do
    sign_in @user
    visit customers_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end
 
    assert :success
  end
end
