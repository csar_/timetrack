require "application_system_test_case"

class InvoicesTest < ApplicationSystemTestCase
include Devise::Test::IntegrationHelpers
  setup do
    @invoice = invoices(:one)
    @project = projects(:one)
    @user = users(:one)
    @customer = customers(:one)
  end

  test "creating a Invoice" do
    sign_in @user
    visit new_customer_invoice_url(@customer)

    fill_in "Cost per hour (€)", with: @invoice.cost_per_hour
    fill_in "Description", with: @invoice.description
    fill_in "Total Hours Worked", with: @invoice.tot_hours
    click_on "Create Invoice"

    visit customer_invoices_url(@customer)
    assert_text @invoice.description
  end

  test "updating a Invoice" do 
    sign_in @user
    visit customer_invoices_url(@customer)
    click_on "Edit", match: :first

    fill_in "Cost per hour (€)", with: @invoice.cost_per_hour
    fill_in "Description", with: @invoice.description
    fill_in "Total Hours Worked", with: @invoice.tot_hours

    click_on "Update Invoice"

    assert :success
  end

  test "destroying a Invoice" do
    sign_in @user
    visit customer_invoices_url(@customer)
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert :success
  end
end
