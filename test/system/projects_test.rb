require "application_system_test_case"

class ProjectsTest < ApplicationSystemTestCase
include Devise::Test::IntegrationHelpers

  setup do
    @project = projects(:one)
    @user = users(:one)
    @customer = customers(:one)
  end

  test "visiting the index" do
    visit root_url
    assert_selector "h1", text: "Welcome to Timetrack!"
    sign_in users(:one)
    assert :success
    visit root_url
    assert_selector "h2", text:"Your Customers:"
  end

  test "creating/updating/destroying a Project" do
    # sign in
    sign_in users(:one)
    visit root_url

    # customer creation
    click_on "New Customer"
    fill_in "Name", with: @customer.name
    click_on "Create Customer"
    assert :success

    # create new project
    click_on "New Project"
    fill_in "Name", with: @project.name
    fill_in "Description", with: @project.description
    click_on "Create Project"
    assert :success
    assert_text @project.name

    # update that project
    click_on "Manage Projects"
    click_on "Edit"
    fill_in "Name", with: @project.name
    fill_in "Description", with: @project.description
    click_on "Update Project"
    assert :success

    # destroy that project
    click_on "Manage Projects"
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_no_text @project.name

  end
  

end
