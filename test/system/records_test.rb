require "application_system_test_case"

class RecordsTest < ApplicationSystemTestCase
include Devise::Test::IntegrationHelpers
   setup do
    @record = records(:one)
    @project = projects(:one)
    @user = users(:one)
    @customer = customers(:one)
  end

  test "creating a Record" do
    sign_in @user
    visit new_project_record_url(@project)
    fill_in "Description", with: @record.description
    fill_in "Hours", with: @record.hours
    click_on "Create Record"
    assert :success

    visit project_records_url(@project,@record)
    assert_text @record.description
    assert_text @record.hours

  end

  test "updating a Record" do
    sign_in @user
    visit project_records_url(@project,@record)
    click_on "Edit", match: :first

    fill_in "Description", with: @record.description
    fill_in "Hours", with: '15'
    click_on "Update Record"
    assert :success

    visit project_record_url(@project,@record)
    assert_text @record.description
    assert_text @record.hours


  end

  test "destroying a Record" do
    sign_in @user
    visit project_records_url(@project)
    page.accept_confirm do
      click_on "Destroy", match: :first
    end
    assert :success

    assert_no_text @record.description

  end
end
