Rails.application.routes.draw do
  get 'site/index'
  devise_for :users
  devise_scope :user do
      get '/users/sign_out' => 'devise/sessions#destroy'
  end

  resources :invoices
  resources :records

  resources :projects do 
    resources :records
    resources :invoices
  end	  

  resources :customers do 
    resources :invoices
    resources :projects
  end

  root 'site#index'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
