# Welcome to TimeTrack!

This is a simple example the way I work developing fast, easy to use webapps.
TimeTrack is a webapp that allows you register time you spend on a project for a certain customer.
It also gives you a nice overview of all your customers' project, time records for each projects, 
and issued invoices for each client.

* System dependencies

In order to run it on your machine you should install ruby on rails environment, please refer to the following link:

https://www.ruby-lang.org/it/documentation/installation/

* Configuration

Download or clone project. Enter projects directory and, once ruby on rails environment is up, the only configuration needed is 
to download project dependencies. Do it by typing: 
```
 $ bundle install
```

* Database creation and initialization

You're still into project directory. Just simply type the following commands:
```
 $ rails db:create
 $ rails db:migrate
```

* Run it!

Launch webapp by typing:
```
 $ rails server
```

It will be up and responding at http://localhost:3000

* How to run the tests

To test models entities:

```
$ rails test test/models
```

To test system actions:

```
$ rails test test/system
```

